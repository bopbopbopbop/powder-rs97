/*
  These are part of Dingoo minimal library by Slaanesh
*/

#include <sys/mman.h>
#include <fcntl.h>

/* These are for Dingoo controller inputs */
#define MASK_GPIO2 	0x00020000 /*00000000000000100000000000000000*/
#define MASK_GPIO3	0x280EC067 /*00101000000011101100000001100111*/
/* This mask is used to select buttons which when combined with the SELECT
 * key create a "new" key */
#define MASK_SEL_SHIFT  0x0009C007 /*00000000000010011100000000000111*/

enum  {
        DINGOO_UP =	1<<6,
	DINGOO_DOWN =	1<<27,
	DINGOO_LEFT =	1<<5,
	DINGOO_RIGHT =	1<<18,
	DINGOO_B =	1<<0,	/* remapped A to match GP2X */
	DINGOO_X =	1<<1,	/* remapped B to match GP2X */
	DINGOO_Y =	1<<19,	/* remapped X to match GP2X */
        DINGOO_A =	1<<2,	/* remapped Y to match GP2X */
	DINGOO_L = 	1<<14,
	DINGOO_R =	1<<15,
	DINGOO_START =	1<<16,	/* actually pin 17, merged gpio2 and gpio3 */
	DINGOO_SELECT =	1<<17,
	DINGOO_POWER =  1<<29,
				/* Virtual keys
				 * SELECT + 'button'
				 * Same as base values except shifted << 7 */
	DINGOO_SEL_B =	(DINGOO_B)<<7,	
	DINGOO_SEL_X =	(DINGOO_X)<<7,
	DINGOO_SEL_Y =	(DINGOO_Y)<<7,
        DINGOO_SEL_A =	(DINGOO_A)<<7,
	DINGOO_SEL_L = 	(DINGOO_L)<<7,
	DINGOO_SEL_R =	(DINGOO_R)<<7,
	DINGOO_SEL_START =(DINGOO_START)<<7
};
